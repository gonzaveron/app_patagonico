import { LOGIN, LOGIN_LOGOUT, UPDATE_USER_NAME } from "../constants/actionTypes"

export const login = data => {
    return {
        type: LOGIN,
        data
    }
}

export const logout = () => {
    return {
        type: LOGIN_LOGOUT
    }
}

export const updateName = name => {
    return {
        type: UPDATE_USER_NAME,
        data: name
    }
}