import React from 'react';
import {Text, View, TextInput, Button} from 'react-native';
import {createStore} from "redux";
import rootReducer from "./reducers/";
import {Provider} from "react-redux";
import {updateName} from "./actions/user"

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default function App() {
  return (
    <Provider store = {store}>
      <View>
        <View style={{flexDirection:"row"}}>
          <Text>Nombre de usuario:</Text>
          <TextInput 
            onChangeText = {(text) => {store.dispatch(updateName(text))}}
            placeholder = "nombre@usuario.com"
            style = {{borderWidth: 1, paddingLeft: 5, marginLeft: 20}}
          />
        </View>
        <View style={{flexDirection: "row"}}>
          <Text>Contraseña:</Text>
          <TextInput 
            placeholder="contraseña"
            style = {{borderWidth: 1, paddingLeft: 5, marginLeft: 70}}
          />
        </View>
        <View style = {{margin: 20,width: 200}}>
          <Button 
            
            title = "Acceder"
          />
        </View>
      </View>
    </Provider>
  );
}