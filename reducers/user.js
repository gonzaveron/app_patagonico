import {
    LOGIN,
    LOGIN_LOGOUT,
    UPDATE_USER_NAME
} from "../constants/actionTypes";

const initialState = {
    user: "", 
    name: "",
    email: "",
    career_id: 0,
    token: "",
  };

export default function loginReducer (state = initialState, action) {
    switch (action.type){
      case LOGIN:
          return {
              ...action.data, 
          };
      case LOGIN_LOGOUT:
          return initialState;
      case UPDATE_USER_NAME:
          return {
              ...state,
              name: action.data,
          }
      default:
          return state
    }
  };